### Hi there 👋, my name is Ahmed Taha
#### I am a senior PHP developer
I have a little involvement in open source software, with a particular interest in web and Cross-platform development. My main focus is to build scripts, apps, and tools to help me get faster in doing my job and finishing my tasks as quickly as possible.

Skills: PHP / NODE.JS / REACT NATIVE/ JS / HTML / CSS

- 🔭 I’m currently working on https://geneuis.sds-sa.dev/
- 🌱 I’m currently learning Vue.js
- 📫 How to reach me: ahmed.taha.ibrahim.94@gmail.com


[<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/github.svg' alt='github' height='40'>](https://github.com/https://gitlab.com/devTaha)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/linkedin.svg' alt='linkedin' height='40'>](https://www.linkedin.com/in/https://www.linkedin.com/in/devahmed94//)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/facebook.svg' alt='facebook' height='40'>](https://www.facebook.com/https://www.facebook.com/engahmedtaha94/)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/twitter.svg' alt='twitter' height='40'>](https://twitter.com/https://twitter.com/a7med_sh3ish3)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/stackoverflow.svg' alt='stackoverflow' height='40'>](https://stackoverflow.com/users/https://stackoverflow.com/users/6555104/ahmed-taha)  

